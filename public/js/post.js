const database = firebase.database();
const para =window.location.href.split('='); 
const block=para[2];
const post_id = para[1].split('&')[0];
var post_title;
window.addEventListener('load',()=>{    
    const back =document.getElementById('back');
    back.addEventListener('click',()=>{
        history.go(-1);
    },false);
    const addNewComment=document.getElementById("addNewComment");
    const forumPic = document.getElementById("forumPic");
    forumPic.addEventListener('click',()=>{
        window.location.href='./index.html';
    },false);
    firebase.auth().onAuthStateChanged((firebaseUser)=>{
        if(firebaseUser){
            online=true;
            const infoRef = firebase.database().ref("users/"+firebaseUser.uid);
            //read user name
            infoRef.once('value', function(snapshot) {
                //console.log(snapshot.child("username").val());
                const username=snapshot.child("username").val();
                if(username!=""&&username!=null)
                    personalInfo.innerText = snapshot.child("username").val();
                else
                    personalInfo.innerText = "無名氏";

                addNewComment.classList.remove("invisible");  
                loginout.innerText="登出";
                personalInfo.setAttribute("href","./personalPage.html");
            });
            //console.log(firebaseUser);
        }else{
            online = false;
            loginout.innerText="登入";
            personalInfo.innerText = "期中作業吧";
            personalInfo.setAttribute("href","./index.html");
            //console.log("not logged in!");
        }
    });
    addNewComment.addEventListener('click',()=>{
        window.location.href = './add_comment.html?'+window.location.href.split('?')[1];
    },false);
    //click login or logout
    loginout.addEventListener("click",(e)=>{
        if(online===true){
            //console.log("log out!");
            firebase.auth().signOut().then(()=>{
                window.location.reload();
            });
        }else{
            window.location = './login.html';
        }
    },false);
    initPost();
},false);   
function initPost(){
    //console.log(block,post_id);
    const postRef = database.ref("/block/"+block+"/posts/"+post_id);

    postRef.once('value',(snapshot)=>{
        readPost(snapshot,post_id);
        initComments(snapshot.child('comments').val());
    }).catch((e)=>{
        alert(e.message);
    });
}

function readPost(post,post_id){
        /*
        <div class='d-flex flex-row border border-secondary p-3' id=post_id>
            <div class="w-25 d-flex flex-column align-items-center" id='author_info'>
                <div class="w-55"><img src="下載.png" class="img-fluid" alt=""></div>
                name
            </div>
            <div class='w-75 pl-2 pr-2 pb-2'>
                <h1 id='post_title' class="title">fdfdfdf</h1>
                <div id='post_content' class="content">1111111111111111111111111111111111111111111</div>
            </div>
        </div>
        */
       const database = firebase.database();
       const post_ele = document.getElementById("post");
       var uid =post.child('uid').val();
       var div0 = document.createElement("div");
       var div00 = document.createElement("div");
       var div01 = document.createElement("div");
       var div000 = document.createElement("div");
       var img0 = document.createElement("img");
       var div010 = document.createElement("div");
       var h10 = document.createElement("h1");
       var title = document.createTextNode(post.child('title').val());
       var content = document.createTextNode(post.child('content').val());
       var author;
       database.ref('/users/'+uid+'/username').once('value',(snapshot)=>{
           author = document.createTextNode(snapshot.val());
           div0.setAttribute('class','d-flex flex-row border border-secondary p-3');
           div0.setAttribute('id',post_id);
   
           div00.setAttribute('class',"w-25 d-flex flex-column align-items-center");
           div00.setAttribute('id','author_info');
   
           div000.setAttribute('class',"w-55");
   
           img0.setAttribute('src',"./下載.png");
           img0.setAttribute('class',"img-fluid");
           img0.setAttribute('alt','...');
   
           div01.setAttribute('class','w-75 pl-2 pr-2 pb-2');

           h10.setAttribute('class','title');
           h10.setAttribute('id','post_title');
           //a global variable
           post_title = document.getElementById(post_title);
   
           div010.setAttribute('class',"content");
           div010.setAttribute('id','post_content');
   
           div0.appendChild(div00);
           div0.appendChild(div01);
   
           div00.appendChild(div000);
           div000.appendChild(img0);
           div00.appendChild(author);
           
           h10.appendChild(title);
           div010.appendChild(content);
   
           div01.appendChild(h10);
           div01.appendChild(div010);
   
           post_ele.appendChild(div0);
       });
}
function initComments(comments){
    const database = firebase.database();
    if(comments!=null){
        //console.log(comments);
        let promises = [];
        for(let comment_id in comments){
            let comment =  comments[comment_id];
            let uid = comment['uid'];    
            promises.push(
                database.ref('/users/'+uid+'/username').once('value').then(username=>{
                    return [username.val(),comment];
                })
            )
        }   
        Promise.all(promises).then((data)=>{
            var count=1;
            for(let i=data.length-1;i>=0;i--){
                //console.log(data[i][0],data[i][1]);
                readComment(data[i][0],data[i][1],count);
                count++;
            }
        });
    }else{
        return;
    }
}
function readComment(username,comment,floor){
    /*
    <div class='d-flex flex-row border-left border-bottom border-right border-secondary p-3' id=comment_id>
        <div class="w-25 d-flex flex-column align-items-center" id='author_info'>
            <div class="w-55"><img src="下載.png" class="img-fluid" alt=""></div>
            name
        </div>
        <div class='w-75 pl-2 pr-2 pb-2'>
            <h5  class="floor">1樓</h1>
            <div class="content">1111111111111111111111111111111111111111111</div>
        </div>
    </div>
    */
    const comments_ele = document.getElementById("comments");
    var div0 = document.createElement("div");
    var div00 = document.createElement("div");
    var div01 = document.createElement("div");
    var div000 = document.createElement("div");
    var img0 = document.createElement("img");
    var div010 = document.createElement("div");
    var h50 = document.createElement("h5");
    var title = document.createTextNode(floor+"樓");
    var content = document.createTextNode(comment['content']);
    var author = document.createTextNode(username);
    div0.setAttribute('class','d-flex flex-row border-left border-bottom border-right border-secondary p-3');
    div0.setAttribute('id',comment.key);

    div00.setAttribute('class',"w-25 d-flex flex-column align-items-center");
    div00.setAttribute('id','author_info');

    div000.setAttribute('class',"w-55");

    img0.setAttribute('src',"./下載.png");
    img0.setAttribute('class',"img-fluid");
    img0.setAttribute('alt','...');

    div01.setAttribute('class','w-75 pl-2 pr-2 pb-2');

    h50.setAttribute('class','floor');

    div010.setAttribute('class',"content");

    div0.appendChild(div00);
    div0.appendChild(div01);

    div00.appendChild(div000);
    div000.appendChild(img0);
    div00.appendChild(author);
    
    h50.appendChild(title);
    div010.appendChild(content);

    div01.appendChild(h50);
    div01.appendChild(div010);

    comments_ele.appendChild(div0);
}
