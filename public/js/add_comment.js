const database = firebase.database();
const para =window.location.href.split('='); 
const block=para[2];
const post_id = para[1].split('&')[0];
//console.log(block,post_id);
window.addEventListener('load',()=>{
    //check user online
    firebase.auth().onAuthStateChanged((firebaseUser)=>{

        if(firebaseUser){
            const submit_yes = document.getElementById("submit_yes");
            const return_back_yes = document.getElementById("return_back_yes");         
            const txtTitle = document.getElementById("txtTitle");   
            return_back_yes.addEventListener("click",()=>{
                history.go(-1);
            },false);
            //click submit
            database.ref('block/'+block+'/posts/'+post_id+'/title').once('value').then((title)=>{
                txtTitle.value = title.val();
            });
            submit_yes.addEventListener("click",submit,false);
        }else{
            history.go(-1);
        }
    });
},false);
function submit(){
    var d =  new Date();
    const txtContent = document.getElementById("txtContent");        
    const uid = firebase.auth().currentUser.uid;
    const timestamp =  d.getTime();
    const content = txtContent.value;
    if(content.length<5){
        alert("請輸入五個字以上");
    }else{
        //post that we want to update
        var newCommentData={
            timestamp:timestamp,
            content:content,
            uid:uid
        }
        const newCommentKey = database.ref("block/"+block+"/posts/"+post_id+'/comments').push().key;
        var updates={};
        updates["block/"+block+"/posts/"+post_id+'/comments/'+newCommentKey]=newCommentData;
        updates['/users/'+uid+'/user-comments/'+newCommentKey]={block:block,post_id:post_id};
        //add new post and add the history to user record
        database.ref().update(updates).then(()=>{
            //replace the current url to improve user experience
            history.go(-1);
        }).catch((e)=>{
            alert(e);
        });
    }
}