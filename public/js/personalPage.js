window.addEventListener("load",()=>{
    const txtName=document.getElementById("txtName");
    const name = document.getElementById("name");
    const save = document.getElementById('save');
    const back  = document.getElementById("back");
    //init user info
    firebase.auth().onAuthStateChanged((u)=>{
        if(u){
            const uid=u.uid;
            const userRef=firebase.database().ref('users/'+uid);
            userRef.child('username').on('value',(snapshot)=>{
                let txtname = snapshot.val();
                //console.log(txtname);
                if(txtname!=null)
                    name.innerText=txtname;
                else   
                    name.innerHTML="無名氏";
            });
            save.addEventListener("click",()=>{
                var updates={username:txtName.value,email:firebase.auth().currentUser.email};
                userRef.update(updates);
            },false);
        }
    });
    //back to previous page
    back.addEventListener('click',()=>{
        history.back();
    },false)
    
},false)