window.addEventListener("load",(e)=>{
    var already_login=true;
    //console.log("load");
    firebase.auth().onAuthStateChanged((firebaseUser)=>{
        if(firebaseUser){
            if(already_login){
                firebase.auth().getRedirectResult()
                .then((result)=>{
                    let user = firebase.auth().currentUser;
                    
                    firebase.database().ref('users/'+user.uid).once('value').then(profile=>{  
                        if(profile){
                            alert("點擊後跳轉回首頁")
                            window.location.replace('./index.html');
                        }else{
                            alert("點擊後跳轉回首頁");
                            writeUserData(user.uid,'新會員',user.email,null);
                        };
                    });
                })
                .catch((e)=>{
                    //console.log(e.message);
                    writeUserData(user.uid,'新會員',user.email,null);
                });
            }
            /*else{
                let user = firebase.auth().currentUser;
                writeUserData(user.uid,"新會員",user.email,null);
            }*/
        }else{
            already_login=false;
            const forumPic = document.getElementById("forumPic");
            forumPic.addEventListener('click',()=>{
                window.location.replace('./index.html');
            },false);
            //console.log("login page");
            const txtEmail = document.getElementById("txtEmail");
            const txtPassword = document.getElementById("txtPassword");
            const sign_in = document.getElementById("sign_in");
            const sign_in_google = document.getElementById('sign_in_google');
            //sign in event
            sign_in.addEventListener("click",()=>{
                event.preventDefault();//we have to cancel the default action of button
                console.log("click sign in");
                const email = txtEmail.value;
                const pass = txtPassword.value;
                const auth = firebase.auth();

                auth.signInWithEmailAndPassword(email,pass).then((user)=>{
                    //replace the current url to improve user experience
                    window.location.replace('./index.html');
                }).catch((e)=>{
                    alert(e.message);
                });
            },false);
            //google sign in
            sign_in_google.addEventListener('click',()=>{
                provider = new firebase.auth.GoogleAuthProvider();
                firebase.auth().signInWithRedirect(provider);
            },false);
        }
    });
    

},false);

function writeUserData(userId, name, email, imageUrl) {
    firebase.database().ref('users/' + userId).update({
        username: name,
        email: email,
        profile_picture : imageUrl,
    }).then(()=>{
        location.replace('./index.html');
    }).catch(e=>{
        alert(e.message);
    });
}