window.addEventListener("load",()=>{
    var online=false;
    const personalInfo = document.getElementById("personalInfo");
    const infoForum = document.getElementById("fInfo");
    const talkForum = document.getElementById("fTalk");
    const loginout = document.getElementById("loginout");
    const forumPic = document.getElementById("forumPic");
    const back =document.getElementById('back');
    back.addEventListener('click',()=>{
        history.go(-1);
    },false);
    forumPic.addEventListener('click',()=>{
        window.location.href='./index.html';
    },false);
    //init user info    
    firebase.auth().onAuthStateChanged((firebaseUser)=>{
        if(firebaseUser){
            online=true;
            const infoRef = firebase.database().ref("users/"+firebaseUser.uid);
            //read user name
            infoRef.once('value', function(snapshot) {
                //console.log(snapshot.child("username").val());
                const username=snapshot.child("username").val();
                if(username!=""&&username!=null)
                    personalInfo.innerText = snapshot.child("username").val();
                else
                    personalInfo.innerText = "無名氏";

                loginout.innerText="登出";
                personalInfo.setAttribute("href","./personalPage.html");
            });
            //console.log(firebaseUser);
        }else{
            online = false;
            loginout.innerText="登入";
            personalInfo.innerText = "期中作業吧";
            personalInfo.setAttribute("href","./index.html");
            //console.log("not logged in!");
        }
    });
    //click login or logout
    loginout.addEventListener("click",(e)=>{
        if(online===true){
            //console.log("log out!");
            firebase.auth().signOut().then(()=>{
                window.location.reload();
            });
        }else{
            window.location = './login.html';
        }
    },false);
    //click 資訊區button to go to Info_forum.html
    infoForum.addEventListener("click",()=>{
        window.location = "./Info_forum.html";
    },false);
    talkForum.addEventListener('click',()=>{
        window.location = "./communicate_forum.html";
    },false);
},false);