const block=window.location.href.split('=')[1];
window.addEventListener('load',()=>{
    //check user online
    firebase.auth().onAuthStateChanged((firebaseUser)=>{
        if(firebaseUser){
            const block_select = document.getElementById("block-select");
            const submit_yes = document.getElementById("submit_yes");
            const return_back_yes = document.getElementById("return_back_yes"); 
            block_select.value = ""+block+"版";
            //click submit
            submit_yes.addEventListener("click",submit,false);
            
            return_back_yes.addEventListener("click",()=>{
                history.go(-1);
            },false);
        }else{
            history.go(-1);
        }
    });
},false)

function submit(){
    var d =  new Date();
    const database = firebase.database();
    const txtContent = document.getElementById("txtContent");
    const txtTitle = document.getElementById("txtTitle");           
    const uid = firebase.auth().currentUser.uid;
    const timestamp =  d.getTime();
    const content = txtContent.value;
    const title = txtTitle.value;
    if(title.length<5||content.length<5){
        alert("標題跟內文請皆輸入五個字以上");  
    }else{
        //post that we want to update
        var newPostData={
            timestamp:timestamp,
            content:content,
            title:title,
            uid:uid
        }
        const newPostKey = database.ref("block/"+block+"/posts").push().key;
        var updates={};
        updates['/block/'+block+'/posts/'+newPostKey]=newPostData;
        updates['/users/'+uid+'/user-posts/'+newPostKey]={block:block};
        //add new post and add the history to user record
        database.ref().update(updates).then(()=>{
            //replace the current url to improve user experience
            history.go(-1);
            
        }).catch((e)=>{
            alert(e);
        });
    }
}