window.addEventListener('load',()=>{
    const addNewPost = document.getElementById("addNewPost");
    const personalInfo = document.getElementById("personalInfo");
    const back =document.getElementById('back');
    back.addEventListener('click',()=>{
        history.go(-1);
    },false);
    const forumPic = document.getElementById("forumPic");
    forumPic.addEventListener('click',()=>{
        window.location.href='./index.html';
    },false);
    //init user info
    firebase.auth().onAuthStateChanged((firebaseUser)=>{
        if(firebaseUser){
            online=true;
            const infoRef = firebase.database().ref("users/"+firebaseUser.uid);
            //read user name
            infoRef.once('value', function(snapshot) {
                //console.log(snapshot.child("username").val());
                const username=snapshot.child("username").val();
                if(username!=""&&username!=null)
                    personalInfo.innerText = snapshot.child("username").val();
                else
                    personalInfo.innerText = "無名氏";
                
                //add new post
                addNewPost.addEventListener("click",()=>{
                    window.location = "./add_post.html?block=communicate"
                },false);
                addNewPost.classList.remove("invisible");
                loginout.innerText="登出";
                personalInfo.setAttribute("href","./personalPage.html");
            });
            //console.log(firebaseUser);
        }else{
            online = false;
            loginout.innerText="登入";
            personalInfo.innerText = "期中作業吧";
            personalInfo.setAttribute("href","./index.html");
            //console.log("not logged in!");
        }
    });
    //click login or logout
    loginout.addEventListener("click",(e)=>{
        if(online===true){
            //console.log("log out!");
            firebase.auth().signOut().then(()=>{
                window.location.reload();
            });
        }else{
            window.location = './login.html';
        }
    },false);   
    //init posts
    initPosts();
    
},false);



function initPosts(){
    const database = firebase.database();
    const posts = database.ref('block/communicate/posts');
    let promises = [];
    $("#posts_table").empty();//clear the posts
    posts.once('value',(snapshot)=>{
        if(snapshot==null){
            throw "no post"
        }else{
            snapshot.forEach((post)=>{
                const uid = post.child('uid').val();
                const usernameRef = database.ref('/users/'+uid).child('username');
                //console.log(post.child('timestamp').val());    
                promises.push(
                    usernameRef.once('value').then((username)=>{
                        //console.log("push");
                        //console.log(post.child('timestamp').val());    
                        return [username.val(),post];  
                    })
                );
            });
            Promise.all(promises).then((data)=>{
                for(var i=data.length-1;i>=0;i--){
                    //console.log(i,data[i][1].child('timestamp').val());
                    readPost(data[i][1],data[i][0]);
                }
                
            });
        }
    }).catch(e=>{
        alert(e.message);
    });
}

function readPost(p,username){
    /*<a href="#" class="list-group-item list-group-item-action">
    <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1 title txtovf" >List group item heading</h5>
        <small>3 days ago</small>
    </div>
    <div class="mb-1 txtovf">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</div>
    <small class="float-right">author name</small>
    </a>*/
    var d = new Date();
    var cur_time = d.getTime();
    var posts_table = document.getElementById("posts_table");
    var post = document.createElement("a");
    var div0 = document.createElement("div");
    var title = document.createElement("h5");
    var timeRecord = document.createElement("small");
    var div1 = document.createElement("div");

    var author = document.createElement("small");
    var authName = document.createTextNode(username);
    var content = document.createTextNode(p.child("content").val());
    var txtTitle = document.createTextNode(p.child("title").val());
    var txtTimeRecord = document.createTextNode(evalTime(cur_time-p.child('timestamp').val())+"前");
    
    post.setAttribute("class","list-group-item list-group-item-action");
    post.setAttribute("id",p.key);
    //post.setAttribute('onclick',"javascript:window.location.href='./post.html?post_id='+this.id; return false;");
    post.addEventListener('click',(e)=>{
        window.location='./post.html?post_id='+e.currentTarget.id+'&block=communicate';
    },false);
    div0.setAttribute("class","d-flex w-100 justify-content-between");
    title.setAttribute("class","mb-1 title txtovf");
    div1.setAttribute("class","mb-1 txtovf");
    author.setAttribute("class","float-right");
    post.appendChild(div0);
    
    title.appendChild(txtTitle);
    timeRecord.appendChild(txtTimeRecord);

    div0.appendChild(title);
    div0.appendChild(timeRecord);

    div1.appendChild(content);
    post.appendChild(div1);

    author.appendChild(authName);
    post.appendChild(author);

    posts_table.appendChild(post);
}
function evalTime(time){
    //the unit of time is ms
    time = time/1000;

    if(time<60){
        return ""+Math.round(time)+"秒";
    }else if(time<3600){
        return ""+Math.round(time/60)+"分";
    }else if(time<86400){
        return ""+Math.round(time/3600)+"小時";
    }else{
        return ""+Math.round(time/86400)+"日";
    }
}

