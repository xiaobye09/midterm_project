window.addEventListener("load",(e)=>{
    //console.log("sign up page");
    const txtEmail = document.getElementById("txtEmail");
    const txtPassword = document.getElementById("txtPassword");
    const txtPasswordCk = document.getElementById("txtPasswordCk");
    const sign_up = document.getElementById("sign_up");
    const forumPic = document.getElementById("forumPic");
        forumPic.addEventListener('click',()=>{
        window.location.replace('./index.html');
    },false);
    //sign up event
    sign_up.addEventListener("click",()=>{
        event.preventDefault();//we have to cancel the default action of button
        //console.log("click sign up");
        const email = txtEmail.value;
        const pass = txtPassword.value;
        const passCk = txtPasswordCk.value;
        const auth = firebase.auth();
        if(pass==passCk){
            auth.createUserWithEmailAndPassword(email,pass).then(()=>{
                alert("註冊成功 點擊後跳轉回首頁");
                writeUserData(auth.currentUser.uid,"新會員",email,null);
            }).catch((e)=>{
                alert(e.message);
            });
        }else{
            alert("請確認密碼正確");
        }
    },false);

},false);

function writeUserData(userId, name, email, imageUrl) {
    firebase.database().ref('users/' + userId).set({
        username: name,
        email: email,
        profile_picture : imageUrl,
    }).then(()=>{
        //replace the current url to improve user experience
        location.replace('./index.html');
    });
}