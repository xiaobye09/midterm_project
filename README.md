# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 期中作業論壇吧
* Key functions (add/delete)
    1. 論壇(完成了個人頁面、首頁、文章列表頁面、文章頁面、留言、發表文章功能)
* Other functions (add/delete)
    1. 改名功能(文章、留言內的名字會同步)
    2. 發文和留言的時候，有emoji按鈕的功能
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-project-bfc26.firebaseapp.com/

# Components Description : 
1. Membership Mechanism : 註冊後會跳出alert點擊後跳轉回首頁，若使用登入來登入的話，登入後一樣會跳出alert後點擊跳轉。避免登入後再次進入登入頁面，有設置監聽登入狀態，若當前已登入，會強制跳轉回首頁。

2. Host on your firebase page : 使用firebase的CLI進行部屬，部屬後的網址如上。

3. Database read&write : 基本上文章中的閱讀權限是開放給所有人的，但是寫入權限，則是留言者或是文章作者才有。每個user會有一個獨立的profile，這個profile只又本人可以寫入，但因為這profile內沒有隱私內容，且原本計畫內是要開放大家能夠互相觀看profile，所以設計成大家都可讀。

4. RWD : 頁面不會有出現文字溢漏的問題，框架跟圖片也會隨著device不同做縮放，大量運用bootstrap。

5. 基本上user page,post page,post list page,leave comment,add new post的功能都有。

6. sign up/in with google : 使用indirect的方式登入，方便某些不支援彈跳視窗的裝置使用。

7. use css animation : 發文和留言的按鈕利用css animation做出動畫效果。
<img src="button.jpg">



# Other Functions Description(1~10%) : 
1. 改名功能 : 能夠支援動態改名。
<img src="改名.jpg">
2. 有文字輸入窗時，都會附上emoji按鈕: 有大量的emoji供選擇。
<img src="emoji功能.jpg">

## Security Report (Optional)
由於使用https所以本身傳送資訊的過程已經確保了資料完整性、資料隱私性、身份認證，對於文章和留言的擁有權。
使用了security rules來避免發文者留言者以外的人對這留言的資料做更動。
取得once下來或on下來的資料也盡量在使用const放在匿名函數中，避免有心人直接從console叫出來看，讓取得較隱私的資料需要透過比較麻煩的步驟。
